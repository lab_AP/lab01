#include <stdio.h>
#include <stdlib.h>

#define DIM 100
void readArr(int *i, int len);
int main(void)
{
  int N1, N2, i, j;
  int v1[DIM];
  float v2[DIM];

  printf("\nN1 : ");
  scanf("%d", &N1);
  printf("\nN2 : ");
  scanf("%d", &N2);
  
  if (N1>DIM || N2>DIM){
    printf("Err: DIM = %d", DIM);
    exit(1);
  }
  
  readArr(v1, N1);

  for (i=0; i<N1; i++) {
    v2[i] = 0;
    int c = 0;
    for(j=-N2; j<=N2; j++){      
      if ((i+j)>=0 && (i+j)<N1){
        v2[i] = v2[i]+v1[i+j];
        c++;
      }
    }
  v2[i] = v2[i]/c;
  }

  for(i=0; i<N1; i++)
    printf("%.3f ", v2[i]);

  return 0;
}


void readArr(int *arr, int len)
{
  int i;
  for(i = 0; i<len; i++)
   scanf("%d", &arr[i]); 
}

