#include <stdio.h>
#include <stdlib.h>

#define R 3
#define C 5

void readMat(int m1[R][C]);
int factorial(int);
int power(int n);

int main(void)
{
  int m1[R][C];
  int m2[R][C];
  int i, j;

  readMat(m1);

  for(i = 0; i<R; i++){
    for(j = 0; j<C; j++){
      if (m1[i][j] == 0)
        m2[i][j] = 0;
      else if (m1[i][j] < 0)
        m2[i][j] = factorial(-m1[i][j]);
      else
        m2[i][j] = power(m1[i][j]);
    }
  }

  for(i = 0; i<R; i++){
    printf("\n");
    for(j = 0; j<C; j++)
      printf("%d ", m2[i][j]);
  }
  return 0;
}

void readMat(int m1[R][C])
{
  int i, j;

  for(i = 0; i<R; i++){
    for(j = 0; j<C; j++){
      printf("\nm1[%d][%d] = ", i, j);
      scanf("%d", &m1[i][j]);
    }
  }
}

int factorial(int n)
{
  if (n==1)
    return 1;
  return(n*factorial(n-1));
}

int power(int n)
{
  int power = 1;
  while(power<=n)
    power = power*10;
  return power;
}
