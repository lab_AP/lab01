#include <stdio.h>
#include <stdlib.h>

#define DIM 15
void readArray(int *);

int main(void)
{
  int i, j;
  int pos;
  int len;
  int max = 0;
  int arr[DIM] = {2,3,4,-1,-10,5,6,8,11,-9,8,-10,9,3,0};
  //readArray(arr);

  for (i = 0; i<DIM; i++){
    len = 1;
    for (j = i; j+1<DIM; j++){
      if(arr[j+1]<arr[j])
        break;
      len++;
      if(len>max){
        pos = i;
        max = len;
      }
    }
  }
  printf("\nmaxpos = %d",pos);
  printf("\nmaxlen = %d\n",max);

  for(i=pos; i<pos+max; i++)
    printf("%d ", arr[i]);

  return 0;

}

void readArray(int *arr)
{
  int i;
  for(i=0; i<DIM; i++)
    scanf("%d", &arr[i]);
}
